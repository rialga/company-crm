## How to Install

1. Clone this Repo 

2. Custom DB at .env File
    ```
     DB_CONNECTION=mysql                   
     DB_HOST=127.0.0.1             
     DB_PORT=3306              
     DB_DATABASE=mini-crm             
     DB_USERNAME=your_username           
     DB_PASSWORD=your_password 

    ```     
 
3. ``` composer install ```


4. ``` php artisan serve ``` 

