@extends('layouts.main')
@extends('layouts.sidebar')

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-4">
                <h1>{{ Auth::user()->company->name }}</h1>
            </div>

        </div>
    </div><!-- /.container-fluid -->
</section>


<section class="content">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ Auth::user()->company->name }} {{ __('text.Employee') }} </i> </h3>
                    </div>
                    <div class="card-body">
                        <table id="t_cemployee" class="table display nowrap">
                            <thead>
                                <tr>
                                    {{-- <th style="width: 10px">No</th> --}}
                                    <th>{{ __('text.Name') }}</th>
                                    <th>Email</th>
                                    <th>{{ __('text.Phone') }}</th>
                                    <th>{{ __('text.Company') }}</th>
                                    <th>{{ __('text.Join Date') }}</th>
                                    <th style="text-align: center">{{ __('text.Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employee_data as $item)
                                    <tr>
                                        <td> {{ $item->first_name }} <b>{{ $item->last_name }}</b> </td>
                                        <td>{{ $item->email== null ? '-' : $item->email }}</td>
                                        <td>{{ $item->phone== null ? '-' : $item->phone }}</td>
                                        <td>{{ $item->companies_id== null ? '-' : $item->company->name }}</td>
                                        <td>
                                            {{ __('text.Date') }}:  {{ \Carbon\Carbon::parse($item->created_at)->setTimezone(Session::get('timezone'))->format('Y-m-d') }}
                                            <br>
                                            {{ __('text.Time') }}: {{ \Carbon\Carbon::parse($item->created_at)->setTimezone(Session::get('timezone'))->format('h:i:s') }}
                                        </td>
                                        <td> - </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $employee_data->links() }}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')

    <script>

    </script>


@endsection
