@section('sidebar')

    <!-- Brand Logo -->
    <a href="#" class="brand-link ml-3">
        <!-- <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
        <i class="fas fa-adjust fa-fw mr-2"></i>
        <span class="brand-text font-weight-bold">{{ env('APP_NAME') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-3">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ url('/home') }}" class="nav-link {{ Request::url() == url('/home') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-columns"></i>
                        <p> {{ __('text.Dashboard') }} </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>

@endsection


