<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Company extends Model
{
    protected $table = 'companies';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'email','logo', 'website' ,'created_by_id' , 'updated_by_id'
    ];

    public function company() {
        return $this->hasMany('App\Employee', 'companies_id', 'id');
    }

    public function createdBy() {
        return $this->belongsTo('App\User', 'created_by_id', 'id');
    }
    public function updatedBy() {
        return $this->belongsTo('App\user', 'updated_by_id', 'id');
    }


    public function scopeSearch($query,$val){
        return $query
        ->where('name','like','%' .$val. '%')
        ->Orwhere('email','like','%' .$val. '%')
        ->Orwhere('website','like','%' .$val. '%')
        ->OrwhereDate('created_at' , $val)
        // ->OrwhereTime('created_at' , Carbon::createFromFormat('H:i:s', '11:38:09', Session::get('timezone'))->setTimezone('UTC'));
        ->OrwhereTime('created_at' , $val);

    }
}
