<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function index()
    {

        $company_id = Auth::user()->companies_id;
        $employee_data = Employee::where('companies_id' , $company_id)->paginate(10);
        return view('home', compact('employee_data'));
    }
}
