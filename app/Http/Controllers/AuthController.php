<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Http\Controllers\Auth\LoginController as DefaultLoginController;
use Illuminate\Support\Facades\Auth;

class AuthController extends DefaultLoginController
{

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest:employee')->except('logout');
    }


    public function showLoginForm()
    {
        return view('welcome');
    }

    public function username()
    {
        return 'email';
    }

    protected function guard()
    {
        return Auth::guard('employee');
    }

}
