<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Authenticatable
{
    protected $table = 'employees';
    protected $guarded = ['id'];
    protected $fillable = [
        'first_name', 'last_name', 'companies_id', 'email', 'phone', 'password', 'created_by_id', 'updated_by_id'
    ];

    protected $hidden = [
        'password',
    ];


    public function getAuthPassword()
    {
     return $this->password;
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'companies_id', 'id');
    }
    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by_id', 'id');
    }
    public function updatedBy()
    {
        return $this->belongsTo('App\user', 'updated_by_id', 'id');
    }

    public function scopeSearch($query, $val)
    {
        return $query
            ->where('first_name', 'like', '%' . $val . '%')
            ->Orwhere('last_name', 'like', '%' . $val . '%')
            ->Orwhere('employees.email', 'like', '%' . $val . '%')
            ->Orwhere('phone', 'like', '%' . $val . '%')
            ->OrwhereDate('employees.created_at', $val)
            ->orWhereHas('company', function ($query) use ($val) {
                $query->where('name', 'like', '%' . $val . '%');
            })
            ->OrwhereTime('employees.created_at', $val);
        // ->OrwhereTime('employees.created_at' , Carbon::parse('H:i:s', $val, Session::get('timezone'))->setTimezone('UTC'));
    }
}
